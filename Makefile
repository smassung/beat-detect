EXENAME = detect
OBJS = analyzer.o callbacks.o beat_detect.o
INC = -I include/
LIBS = -L lib/libm.a lib/libFLAC.a lib/libFLAC++.a lib/libogg.a -lfftw3

CXX = g++
CXXOPTS = -O3 -Wall -pedantic -std=c++11 -fopenmp
#CXXOPTS = -g -O0 -Wall -pedantic -std=c++11
LINKER = g++

all: $(EXENAME)

$(EXENAME): main.cpp $(OBJS)
	$(LINKER) $(CXXOPTS) main.cpp -o $(EXENAME) $(OBJS) $(INC) $(LIBS)

%.o : %.cpp %.h
	$(CXX) $(CXXOPTS) -c $< -o $@ $(INC)

clean:
	rm -f $(EXENAME) *.o
