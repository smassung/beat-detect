/**
 * @file analyzer.cpp
 */

#include <iostream>
#include <string.h>
#include "analyzer.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;

Analyzer::Analyzer():
    _num_bytes(0),
    _processed(0),
    _input(NULL),
    _pcm(vector<double>()){ /* nothing */ }

size_t Analyzer::length() const
{
    return _pcm.size();
}
    
vector<double> Analyzer::normalized() const
{
    vector<double> norm;
    norm.reserve(_pcm.size());

    for(auto & d: _pcm)
        norm.push_back((d + 32768) / 65535);

    return norm;
}

FLAC__StreamDecoderReadStatus Analyzer::read_callback(FLAC__byte buffer[], size_t* bytes)
{
    size_t diff = _num_bytes - _processed;

    if(diff == 0)
        return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;

    if(diff < *bytes)
        *bytes = diff;

    if( memcpy(buffer, _input +  _processed, *bytes) != nullptr )
    {
        _processed += *bytes;
        return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
    }

    return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
}

FLAC__StreamDecoderWriteStatus
Analyzer::write_callback(const FLAC__Frame *frame, const FLAC__int32 * const buffer[])
{
    // only save left channel (left is buffer[0])
    for(size_t i = 0; i < frame->header.blocksize; ++i)
        _pcm.push_back(static_cast<double>(buffer[0][i]));

    return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void Analyzer::error_callback(FLAC__StreamDecoderErrorStatus status)
{
    std::cerr << "in error_callback" << std::endl;
}
