/**
 * @file analyzer.h
 */

#ifndef _ANALYZER_H_
#define _ANALYZER_H_

#include <string>
#include <vector>
#include "FLAC++/decoder.h"

class Analyzer: public FLAC::Decoder::File
{
    public:

        Analyzer();

        size_t length() const;

        /**
         * @return a normalized vector of waveform data in the range [0, 1]
         */
        std::vector<double> normalized() const;

    protected:

        virtual FLAC__StreamDecoderReadStatus
        read_callback(FLAC__byte buffer[], size_t* bytes);

	    virtual FLAC__StreamDecoderWriteStatus
        write_callback(const FLAC__Frame *frame, const FLAC__int32 * const buffer[]);

        virtual void error_callback(FLAC__StreamDecoderErrorStatus status);

    private:

        size_t _num_bytes;
        size_t _processed;
        FLAC__byte* _input;
        std::vector<double> _pcm;
};

#endif
