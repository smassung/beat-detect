#include <iostream>
#include <cmath>
#include <sstream>
#include <fstream>
#include <limits>
#include "beat_detect.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;

vector<double> BeatDetect::do_real_fft(const vector<double> & points)
{
    vector<double> output(points.size());
    fftw_plan plan = fftw_plan_r2r_1d(
            points.size(),
            // this function will not modify the data, but can't take a const
            // value as a parameter
            const_cast<vector<double> &>(points).data(),
            output.data(),
            FFTW_REDFT10,   // REDFT10 transform, i.e. a DCT-II
            FFTW_ESTIMATE   // don't try to optimize
    );
    fftw_execute(plan);
    fftw_destroy_plan(plan);
    return output;
}

void BeatDetect::do_inverse_real_fft(vector<vector<double>> & bands)
{
    for(auto & band: bands)
    {
        fftw_plan plan = fftw_plan_r2r_1d(
                band.size(),
                band.data(),
                band.data(),   // in-place, since same location
                FFTW_REDFT01,  // inverse of DCT-II transform
                FFTW_ESTIMATE
        );
        fftw_execute(plan);
        fftw_destroy_plan(plan);
    }
}

vector< vector<double> > BeatDetect::get_bands(const vector<double> & points)
{
    vector<double> transformed = do_real_fft(points);
    vector<double> limits = {0, 200, 400, 800, 1600, 3200};

    double maxfreq = 4096;
    size_t num_bands = limits.size();
    size_t n = points.size();
    vector<size_t> left(n);
    vector<size_t> right(n);

    // determine beginning and end of band data from FFT
    for(size_t i = 0; i < num_bands - 1; ++i)
    {
        left[i] = floor(limits[i] / maxfreq * n / 2);
        right[i] = floor(limits[i + 1] / maxfreq * n / 2) - 1;
    }
    left[num_bands - 1] = floor(limits[num_bands - 1] / maxfreq * n / 2);
    right[num_bands - 1] = floor(n / 2) - 1;

    // allocate all frequency bands, initializing to 0
    vector<vector<double>> bands;
    for(size_t i = 0; i < num_bands; ++i)
        bands.emplace_back(vector<double>(points.size(), 0.0));

    // split up original FFT output to correct band location
    for(size_t i = 0; i < num_bands; ++i)
    {
        for(size_t j = left[i]; j < right[i]; ++j)
            bands[i][j] = transformed[j];

        for(size_t j = n - right[i] - 1; j < n - left[i] - 1; ++j)
            bands[i][j] = transformed[j];
    }

    // remove garbage value
    bands[0][0] = 0.0;

    // invert the processed FFT data in each band to get separated signals
    do_inverse_real_fft(bands);

    return bands;
}

void BeatDetect::lowpass_filter(vector<double> & points)
{
    // make everything negative 0
    for(auto & p: points)
    {
        if(p < 0)
            p = 0;
    }

    // how many iterations of smoothing to run
    size_t n = 5;
    
    // how far back to look in the average
    size_t lag = 2000;

    vector<double> output(points.size(), 0.0);
    for(size_t k = 0; k < n; ++k)
    {
        #pragma omp parallel for
        for(size_t i = lag; i < points.size(); ++i)
        {
            double sum = 0.0;
            for(size_t j = 0; j < lag; ++j)
                sum += points[i - j];
            output[i] = sum / lag;
        }
        points = output;
    }
}

double BeatDetect::find_bpm(const vector<double> & points)
{
    size_t sample_rate = 44100;
    size_t width = sample_rate * 3;
    vector<double> window(points.begin(), points.begin() + width);
    vector<double> wave(points.begin() + width, points.end());
    
    // move window this amount each iteration
    size_t inc = 4;

    size_t best_offset = 0;
    double best_match = std::numeric_limits<double>::max();
    
    for(size_t i = 0; i < width; i += inc)
    {
        double match = match_waves(wave, window, i);
        if(match < best_match)
        {
            best_match = match;
            best_offset = i;
        }
    }

    return (static_cast<double>(best_offset) / width) * 180;
}

double BeatDetect::match_waves(const vector<double> & wave, const vector<double> & window, size_t offset)
{
    double sum = 0;
    for(size_t i = 0; i < window.size(); ++i)
        sum += std::abs(wave[offset + i] - window[i]);
    return sum;
}

void BeatDetect::plot(const vector<double> & points, size_t num)
{
    std::ostringstream ss;
    ss << num;
    string filename = "band-" + ss.str() + ".dat";
    cerr << " Creating plot \"" << filename << "\"" << endl;
    std::ofstream out(filename.c_str());
    
    for(auto & p: points)
        out << p << endl;

    out.close();
}
