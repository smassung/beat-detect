/**
 * @file beat_detect.h
 */

#ifndef _BEAT_DETECT_H_
#define _BEAT_DETECT_H_

#include <fftw3.h>
#include <vector>

/**
 * Contains various functions used for beat detection from PCM data.
 */
namespace BeatDetect
{
    /**
     * Separates a waveform into multiple frequency bands.
     * @param points The original waveform
     * @return new waveforms in each frequency range
     */
    std::vector< std::vector<double> > get_bands(const std::vector<double> & points);

    /**
     * Creates a plot of points on a waveform.
     * @param points The points to plot
     * @param num ID for filename
     */
    void plot(const std::vector<double> & points, size_t num);

    /**
     * Computes a real discrete Fourier transform on a series of points.
     * @param points The data to transform
     * @return the transformed points
     * @see
     * http://www.fftw.org/fftw3_doc/Real_002dto_002dReal-Transform-Kinds.html
     */
    std::vector<double> do_real_fft(const std::vector<double> & points);

    /**
     * Computes the inverse real discrete Fourier transform.
     * @param bands A vector of frequency bands to invert. The computation is
     * done in-place.
     * @see
     * http://www.fftw.org/fftw3_doc/Real_002dto_002dReal-Transform-Kinds.html
     */
    void do_inverse_real_fft(std::vector<std::vector<double>> & bands);

    /**
     * Runs a lowpass filter to smooth the signal down to its envelope.
     * @param points The signal to smooth
     */
    void lowpass_filter(std::vector<double> & points);

    /**
     * @param points The data to analyze for beats. This function assumes the
     * tempo is constant throughout the song.
     * @return the estimated tempo of the song
     */
    double find_bpm(const std::vector<double> & points);

    /**
     * @param wave
     * @param window
     * @param offset
     */
    double match_waves(const std::vector<double> & wave, const std::vector<double> & window, size_t offset);
}

#endif
