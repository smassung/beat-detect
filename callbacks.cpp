/**
 * @file callbacks.cpp
 */

#include "callbacks.h"

FLAC__StreamDecoderReadStatus
Callbacks::read_callback(const FLAC__StreamDecoder* decoder,
        FLAC__byte* buffer, size_t* bytes, void* client_data)
{
    /*
    size_t dif = num_bytes - bytes_processed;
    
    if(dif == 0)
        return FLAC__STREAM_DECODER_READ_STATUS_END_OF_STREAM;
    
    if(dif < *bytes)
        *bytes = dif;    // if we're at the end, we probably won't have a complete block
    
    if( memcpy(buffer, client_data + bytes_processed, *bytes) != NULL )
    {
        bytes_processed += *bytes;        
        return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
    }
    */
            
    //return FLAC__STREAM_DECODER_READ_STATUS_ABORT;
    return FLAC__STREAM_DECODER_READ_STATUS_CONTINUE;
}

FLAC__StreamDecoderWriteStatus
Callbacks::write_callback(const FLAC__StreamDecoder* decoder,
        const FLAC__Frame* frame, const FLAC__int32* const buffer[], void* client_data)
{
    /*
    if(total_samples == 0)
    {
        cerr << "ERROR: analyzer only works for FLAC files that have a total_samples count in STREAMINFO\n";
        return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }

    if(channels != 2 || bps != 16)
    {
        cerr << "ERROR: analyzer only supports 16-bit stereo streams\n";
        return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }
    */

    for(size_t i = 0; i < frame->header.blocksize; ++i)
    {
        //int amplitude = (buffer[0][i]);   // only look at the left channel
    }

    return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void Callbacks::error_callback(const FLAC__StreamDecoder* decoder,
        FLAC__StreamDecoderErrorStatus status, void* client_data)
{
    std::cerr << "Got error callback: "
         << FLAC__StreamDecoderErrorStatusString[status] << ". Will attempt to continue..."
         << std::endl;
}
