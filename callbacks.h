/**
 * @file callbacks.h
 */

#ifndef _CALLBACKS_H_
#define _CALLBACKS_H_

#include <iostream>
#include "FLAC++/decoder.h"
#include "FLAC++/metadata.h"

namespace Callbacks
{
    FLAC__StreamDecoderReadStatus
    read_callback(const FLAC__StreamDecoder* decoder,
        FLAC__byte* buffer, size_t* bytes, void* client_data);

    FLAC__StreamDecoderWriteStatus
    write_callback(const FLAC__StreamDecoder* decoder,
        const FLAC__Frame* frame, const FLAC__int32* const buffer[], void* client_data);

    void error_callback(const FLAC__StreamDecoder* decoder,
        FLAC__StreamDecoderErrorStatus status, void* client_data);
}

#endif
