/**
 * @file main.cpp
 */

#include <iostream>
#include <vector>
#include "FLAC/all.h"
#include "FLAC++/decoder.h"
#include "FLAC++/metadata.h"
#include "analyzer.h"
#include "beat_detect.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;


int main(int argc, char* argv[])
{
    Analyzer analyzer;
    FLAC__StreamDecoderInitStatus status = analyzer.init(argv[1]);
    if(status != FLAC__STREAM_DECODER_INIT_STATUS_OK)
    {
        cerr << "Error initializing file!" << endl;
        return 1;
    }

    cerr << " Decoding FLAC audio..." << endl; 
    analyzer.process_until_end_of_stream();

    cerr << " Size uncompressed: "
         << (analyzer.length() * (sizeof(double)) * 2 /* both channels */) / (1024 * 1024)
         << "MB" << endl;

    cerr << " Normalizing PCM data on [0, 1]..." << endl; 
    vector<double> points = analyzer.normalized();

    cerr << " Separating frequencies with FFTW..." << endl; 
    vector< vector<double> > bands = BeatDetect::get_bands(points);

    cerr << " Running lowpass filter on frequency bands..." << endl;
    for(auto & band: bands)
      BeatDetect::lowpass_filter(band);

    cerr << " Calculating best BPM..." << endl;
    for(auto & band: bands)
        cout << BeatDetect::find_bpm(band) << endl;

    size_t i = 0;
    for(auto & band: bands)
        BeatDetect::plot(band, i++);

    return 0;
}
